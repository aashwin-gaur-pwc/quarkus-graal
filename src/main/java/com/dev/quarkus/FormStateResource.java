package com.dev.quarkus;

import com.dev.quarkus.model.FormState;
import com.dev.quarkus.service.FormStateService;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

@Path("/form")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Slf4j
public class FormStateResource {

    @Inject
    FormStateService formStateService;

    @GET
//    @PermitAll
    public FormState getFormState(@Context SecurityContext securityContext) {
        Principal caller = securityContext.getUserPrincipal();
        log.info("Caller - " + caller);
        log.info("isSecure - " + securityContext.isSecure());
        String idFromContext = "test-guid";
        return formStateService.getForm(idFromContext)
                .thenApply(s -> FormState.fromAVM(s.item())).join();
    }

    @PUT
//    @RolesAllowed("USER")
    public FormState updateForm(@Valid @NotNull FormState state) {
        log.info("Put method called");
        return formStateService.updateFormState(state);
    }

}