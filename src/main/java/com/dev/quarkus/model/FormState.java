package com.dev.quarkus.model;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@AllArgsConstructor
@RegisterForReflection
@NoArgsConstructor
@Data
public class FormState {

    @NotBlank
    private String id;

    @NotBlank
    private String clid;

    private Set<String> tags;

    public static FormState fromAVM(Map<String, AttributeValue> avm) {
        FormState state = new FormState();
        state.setId(avm.get("id").s());
        state.setClid(avm.get("clid").s());
        state.setTags(new HashSet<>(avm.get("tags").ss()));
        return state;
    }


}
