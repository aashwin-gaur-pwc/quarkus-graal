package com.dev.quarkus.service;

import com.dev.quarkus.model.FormState;
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

@Singleton
public class FormStateService {

    @Inject
    DynamoDbAsyncClient dynamoDbAsyncClient;

    public CompletableFuture<GetItemResponse> getForm(String id) {
        return dynamoDbAsyncClient.getItem(GetItemRequest.builder()
                .tableName("forms")
                .key(Collections.singletonMap("id", AttributeValue.builder().s(id).build()))
                .build());
    }
    
    public FormState updateFormState(@Valid FormState state) {
        return state;
    }

}
