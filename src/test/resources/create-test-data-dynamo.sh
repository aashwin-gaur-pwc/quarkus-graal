
#docker run --rm --name local-dynamo -p 8000:4569 -e SERVICES=dynamodb -e START_WEB=0 -d localstack/localstack

docker rm $(docker stop $(docker ps -a -q --filter ancestor=amazon/dynamodb-local --format="{{.ID}}"))
docker run -p 8000:8000 amazon/dynamodb-local -jar DynamoDBLocal.jar -sharedDb &

sleep 3

aws dynamodb create-table --table-name forms \
    --attribute-definitions AttributeName=id,AttributeType=S \
    --key-schema AttributeName=id,KeyType=HASH \
    --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1 \
    --profile localstack --endpoint-url=http://localhost:8000 > /dev/null 2>&1

aws dynamodb wait table-exists \
    --table-name forms \
    --profile localstack --endpoint-url=http://localhost:8000

aws dynamodb put-item --table-name forms \
    --item '{"id": {"S":"test-guid"}, "clid": {"S":"test-clid"}, "tags": {"SS":["tag1","tag2"]} }' \
    --profile localstack --endpoint-url=http://localhost:8000
